# Workstation Setup:
This repository holds all information and files for a setup of the institute-workstation under Kubuntu 18.04 LTS.

## Folder-Structure
- **Readme:** Holds all information about the system and its installation.
- **CGroups_Configuration:** Holds all necessary files for CGroups-Configuration.
- **MatlabShortcuts:** Holds all necessary files to create Matlab-Shortcuts on the desktop or start-menu.

## Installed Hardware
At the moment we have at the institute two workstations (generation 1 and generation 2) with the same software-setup but different hardware. Workstation 2 is newer and is roughly two times as fast as workstation 1.

### Workstation 1
- **SSD:** Samsung PM1725a MZPLL3T2HMLS - Solid-State-Disk - 3.2 TB - intern - HHHL (HHHL) - PCI Express 3.0 x8 (NVMe)
- **CPU:** AMD Ryzen Threadripper 2990WX (TR4, 3GHz, Unlocked)
- **Motherboard:** ASUS ROG STRIX X399-E GAMING (TR4, AMD X399, E-ATX)
- **Fans:** Gehäuselüfter Corsair ML140
- **Cooler:** Kühler Noctua NH-U14S TR4-SP3
- **Housing:** CORSAIR Carbide Series Quiet 400C (CC-9011081-WW)
- **Power Supply:** Netzteil Corsair, CX Serie 750W
- **Graphics:** Gainward RTX2080 Ti Phoenix GS, 11GB GDDR6
- **RAM:** G.Skill Flare X DDR4-2933MHz CL16-16-16-36 1.35V 128GB (8x16GB)
- **LAN-Card:** Edimax EN-9320SFP+ (Chipsatz TN4010 MAC from Tehuti Networks), SFP+ Netzwerkkarte

### Workstation 2
- **SSD:** Samsung MZPLL3T2HAJQ-00005 - Solid-State-Disk - 3.2 TB - intern - HHHL (HHHL) - PCI Express 3.0 x8 (NVMe)
- **CPU:** AMD Ryzen Threadripper 3970X (3.70GHz / 128MB)
- **Motherboard:** Asus ROG Zenith II Extreme
- **Fans:** Gehäuselüfter Corsair ML140
- **Cooler:** Kühler Noctua NH-U14S TR4-SP3
- **Housing:** ASUS TUF Gaming GT501 Case
- **Power Supply:** Netzteil Enermax MaxTytan, EDT1050EWT,1050
- **Graphics:** Gainward RTX2080 Ti Phoenix GS, 11GB GDDR6
- **RAM:** G.Skill Trident Z RGB DDR4-3200MHz CL14-14-14-34 1.35V
128GB (8x16GB) (F4-3200C14Q2-128GTZR)



**Remarks:**  We have noticed, that for reasons of stability it is important for quadro-channel-CPU's, that you buy at least quadro-packages of RAM-sticks or if you want to use all 8 DIMM-slots, that you buy a octa-package of RAM-sticks! It is further important to make a memory-test to check, that the memory is working properly. This can be performed using `memtest86+`. This tool is installed by default on an Ubuntu-installation or can be started from a live-system-USB-stick. To start the test, a reboot of the machine is necessary and after accessing the GRUB menu, select `Memory test (memtest86+)` (see figure below). If the GRUB menu is not showing up by default during startup, one has to press the `Esc-key` (maybe several times), after Uefi has started-up (Caution, if BIOS quick start is enabled, you may have no time to press the `Esc-key` and therefore this has to be deactivated firstly in the BIOS).

If the memory-test finds some problems, one reason could be wrong RAM-settings in the BIOS, for example too low cl-timings or a too high RAM-clock-frequency.

![GRUB menu](./Images/boot_grub.png)

## Installed OS and its Configuration
### OS-Installation
We've decided to install [`Kubuntu 18.04 LTS`](https://kubuntu.org/getkubuntu/), because `KDE Plasma` is, in contrast to the `Unity-Desktop-Environment`, supported by most RemoteDesktop-Solutions for Linux.

#### Swap-Partition
About the installation of the `Kubuntu 18.04 LTS` is not much to say. The only important point is to create during the installation-procedure a `swap-partition`. Like mentioned [here](https://itsfoss.com/swap-size/), the size of a swap-partition for a system with 128GB of RAM should be around 11GB on an Ubuntu-System.

### Driver-Installation for 10G SFP+ Network-Adapter
You will find the drivers for the 10G SFP+ Network-Adapter [here](https://github.com/acooks/tn40xx-driver) and the installation-instructions [here](https://github.com/acooks/tn40xx-driver/blob/release/tn40xx-001/docs/dkms.md).

#### Installation:

    git clone -b release/tn40xx-001 https://github.com/acooks/tn40xx-driver.git/usr/src/tn40xx-001
    dkms add -m tn40xx -v 001
    dkms install -m tn40xx -v 001

#### Deinstallation:

    dkms remove -m tn40xx -v 001 --all

### Setup multiple Users
To create a normal user without admin-privileges, use the following command, where `<new-user-name>` has to be replaced with the actual user-name you wish to create:

    sudo adduser <new-user-name>

Complete the several steps for creating the user which is including creating the password and user personal information.

To create a user with admin-privileges, the first step is the same like when creating a user without admin-privileges:

    sudo adduser <new-user-name>

Afterwards you have to add the user to the `sudo` group:

    sudo usermod -aG sudo <new-user-name>

Now you are finished!

On the current system were the following users created:

- User: dsvadmin  
  Password: 1234

- User: sia1  
  Password: 1234

- User: vtr1  
  Password: 1234

- User: gab2  
  Password: 1234

#### Change password of a users
To change the password of a user, just type the following command in a terminal and Replace `<username>` with the user you want to change the password for:

    sudo passwd <username>

After you tiped in your password to get root-privileges (if you have it not already) you will be asked to type in the new password once and a second time for confirmation. Your done, the new password for a specific user should be set!

### Setup Remote-Desktop
We've decided to use X2Go for Remote-Desktop because it is easy to use and allows multiple users to work at the same time on the machine from remote.

#### Installing X2Go-Server:
On the Workstation X2Go-Server has to be installed like described [here](https://wiki.x2go.org/doku.php/doc:installation:x2goserver).

Because X2Go does not support the `GNOME 3.x Desktop Environment`, `KDE Desktop Environment` has to be installed in addition like described [here](https://wiki.ubuntuusers.de/KDE_Installation/):

    sudo apt-get install kubuntu-desktop

For the workstation 2, following procedure was applied [see also](https://fabianlee.org/2019/02/03/ubuntu-x2go-on-ubuntu-bionic-for-remote-desktop-access/):

    sudo apt-add-repository ppa:x2go/stable

    sudo apt-get update

    sudo apt-get install x2goserver x2goserver-xsession

    sudo apt-get install plasma-widget-x2go



    sudo apt-get install xfce4 xfce4-terminal

    sudo apt-get install gnome-icon-theme tango-icon-theme


#### Installing X2Go-Clients:
Each user who wants to use the workstation has to install on his machine first the X2Go-Client application, which can be found [here](https://wiki.x2go.org/doku.php/download:start).

After the installation the session-presets have to be set:
- The `session name` is not very important and only useful to distinguish between different sessions-presets.
- Under `Host` you have to type in the fix IP of the Workstation.
- Under `Login` you have to type in your user-name. In this case it is `sia1`.
- Under `SSH port` you have to type in the port on which the Workstation is listening for ssh-connections. Normally it is 22.
- Under `Session type` you have to chose `KDE`, because this is the installed Desktop-Environment on the Workstation.

![SessionSettings](./Images/X2Go_Session.png)

To make sure that the Remote-Desktop-Connection is enough responsive, go under the tab `Connection` and set the `Connection speed` to `LAN`:

![SessionSettings](./Images/X2Go_Connection.png)

In a last step you can set under the tab `Input/Output` your preferred display-size. It can be sometimes tricky to find the optimal display-settings. After you set the display here, you have to finish the setup with `OK` and start your first session! Make sure that you are in the BFH-Network, either directly or over a VPN-connection.  

![SessionSettings](./Images/X2Go_InputOutput.png)

At your first login, the windows-size may not fit your screen and you have to change the screen-settings in the workstations-system. For that, try to open the settings under `Application Louncher` -> `Applications` -> `Settings` -> `System Settings` and choose `Display and Monitor`. Here you can change the monitor-resolution. Be aware, that to see the final effect of your new monitor-settings, you have to close your Remote-Desktop-Session and reopen it! If you can't reach the `System Settings` over the GUI, you can press `Alt+F2` or `Alt+Space` or type in the following command in a terminal:

    systemsettings5 &

#### Reach the Workstation from outside the bfh
The workstation can not be reached directly from outside the BFH-network over an own public address. However, if you firstly connect to the BFH-VPN, the workstation can be reached over the same IP as configured above in X2Go!

#### Troubleshooting
We encountered sometimes problems to reconnect to the remote computer, when we not properly closed a running X2Go session the last time. In addition, this problem remained after a restart of the X2Go client as well as a restart of the remote computer. A workaround for this problem was finally to start a new session in the X2Go client with an other Desktop environment than `KDE` (e.g. `GNOME`). This will in the end fail to connect, but afterwards you can change the client back to `KDE` and it will connect agin without problems!

### Setup Matlab
#### Installation
Because of the Matlab-licences we found no way to install Matlab one time as root for all users. Therefore it is necessary to install Matlab for each user separately under `~/MATLAB` or for example for the Matlab-Version R2018b under `~/MATLAB/R2018b` if one wants to install later other versions of Matlab. Therefore one should also not execute the Matlab-installer under root!

#### Desktop-/Start-Menu-Launcher
To get a Desktop-Launcher and/or Start-Menu-Launcher the following steps are needed:
- Go to the repository-folder `MatlabLauncher` and copy the folder `matlabicon` to your Matlab-installation-path e.g. `~/MATLAB` or `~/MATLAB/R2018b` on the workstation. Make sure, that the folder and its content is readable!

- Open the file `Matlab.desktop` in the repository-folder `MatlabLauncher` and replace the username in the path on line `5` and `8` with yours. Save the file under `~/Desktop/` (for a desktop-launcher) or under `~/.local/share/applications/` (for start-menu-launcher). If the folder `~/.local/share/applications/` does not already exist, create it!
- Make sure, that the files are executable!

#### Matlab tuning
When using Matlab for parallel-computing, under `Preferences -> Parallel Computing Toolbox` the preferred number of Workers in a parallel pool can be defined. One has to play with the number an test, which number is optimal!

An other setting, especially if one observes `OutOfMemoryError: Java heap space` errors, can be found under `Preferences -> General -> Java Heap Memory`. There the heap could be increased to the maximum! If the maximum is still not enough and there is more RAM available, the maximum can be increased in the Java settings-file `~/.matlab/<matlav-version>/matlab.prf` (`<matlav-version>` has to be replaced with the actual installed Matlab-version). In the file one should find a variable named `JavaMemHeapMax`. There the maximal value can be increased! Afterwards a restart of Matlab is needed! If you cant find the file, because it is stored from Matlab at an other place, you can get the right path with the command `prefdir` in the Matlab-console.

#### Limiting the resources for MATLAB
Matlab can be very resource-consuming and could crash the system, if not moderated! Especially on a multi-user-system like this one, this has to be prevented. For this cases linux-systems introduces the so called `CGroups`. With CGroups it is possible to group running processes on the system and to limit the available resources for a specific group. In our case we will assign all running Matlab-processes to a specific Matlab-CGroup and limit its maximal allocable CPU, RAM and Swap-resources:

1. Open the file `cgconfig.conf` in the Repository folder `CGroups_Configuration`. In this file you will find a defined group named `app`. You can rename it if you want, but make sure, that you will rename the group also in the file `cgrules.conf`. In our case the CPU has 32 Cores (64 softcores because of hyper-threading) with a quadro-channel-configuration for the memory and four NUMA-Nodes. Unfortunately only two NUMA-Nodes have direct access to the memory (0 and 2). We allowed Matlab to use the softcores 0 - 59 and to allocate 100GB of memory in maximum. The maximal usable Swap-memory for Matlab is restricted to 5GB. You can change this parameters if you want. Important to note in this context is, that the swap-memory is defined as the RAM plus the Swap! So in our case this would mean `memory.memsw.limit_in_bytes = 105G`. At the moment it is not possible to define cgroups-subgroups like `app/matlab`! The reason for this problem is not entirely clear. Either it is a bug of the used cgroup-tools or we haven't understand all mechanisms enough well. Further information about CGroups can you find [here](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/sec-cpu), [here](https://wiki.archlinux.org/index.php/cgroups) and [here](https://github.com/jooyong-park/workspace/wiki/cgroups-on-debian-ubuntu).

2. Copy the two files `cgconfig.conf` and `cgrules.conf` to `/etc/`.

3. Install the cgroup-tools:

        sudo apt-get install cgroup-tools

4. Debian, by default, disables the memory controller. Do enable it, we can add the following line to the file `/etc/default/grub`:

        GRUB_CMDLINE_LINUX_DEFAULT="quiet cgroup_enable=memory cgroup_enable=cpuset swapaccount=1"

5. Afterwards it is necessary to update grub:

        sudo update-grub

6. The last step is to copy the service-file `cgroups.service` in the repository-folder `CGroups_Configuration` to `/etc/systemd/system`, activate and start the service:

        sudo systemctl enable cgroups.service
        sudo systemctl start cgroups.service

Now the cgroup-tool will be started after each reboot automatically!

### Setup R and R-Studio
#### Installing R
...

#### Installing R-Studio
To install R-Studio some workarounds are necessary due to a conflict with X2Go. To do so, we have to install a wrapper/proxy library provided by Mesa to get around the conflict with X2Go:

1. In a first step it is necessary to make it possible to work with `aptitude`. To do so, open `/etc/apt/sources.list`, e.g. using

        sudo nano /etc/apt/sources.list

    and ensure that the deb-src lines are not commented out.

    Example: You need to change

        deb http://archive.ubuntu.com/ubuntu artful main restricted
        # deb-src http://archive.ubuntu.com/ubuntu artful main restricted

    to

        deb http://archive.ubuntu.com/ubuntu artful main restricted
        deb-src http://archive.ubuntu.com/ubuntu artful main restricted

    Afterwards it is necessary to make sure that `apt-get` has adopted the changes by executing

        sudo apt-get update

2. Now you are ready to install the wrapper/proxy library by executing the following lines:

        sudo aptitude build-dep mesa
        sudo aptitude install scons llvm-dev
        sudo apt-get source mesa
        cd mesa
        sudo scons libgl-xlib

3. If everything in the last step worked fine, the wrapper/proxy library should now be installed and the last step is to make the new installed library available for the system be including the path into the environmental-variable `LD_LIBRARY_PATH`. To make this setting system-wide and permanent, you have to write the path into the file `/etc/environment`. To do so, open the file, e.g. using:

        sudo nano /etc/environment

    If you can already find in the file the path-variable `LD_LIBRARY_PATH`, then include the following line at the end of expression behind the `LD_LIBRARY_PATH`, separated by `:` :

        LD_LIBRARY_PATH="<already-existing-paths>:/etc/<mesa-folder>/build/linux-x86_64-debug/gallium/targets/libgl-xlib/"

    where `<mesa-folder>` has to be replaced with the actual name of the mesa-folder (with correct version-number) under `/etc`.

    If in the file `/etc/environment` the environmental-variable `LD_LIBRARY_PATH` is not already existent, just include the following line on a new line in the file:

        LD_LIBRARY_PATH="/etc/<mesa-folder>/build/linux-x86_64-debug/gallium/targets/libgl-xlib/"

    where `<mesa-folder>` has to be replaced with the actual name of the mesa-folder (with correct version-number) under `/etc`.

    Afterwards you have to restart the system. If everything went well, the following terminal-command should print a version higher than `1.2`:

        glxinfo | grep 'GLX version:'

3. After everything in the last two steps worked without problems, you are now able to install R-Studio:

        sudo apt-get install <path-to-downloaded-deb-package-from-R-Studio>

### Installing Dropbox for a certain user
Installing Dropbox for the first user on the workstation is quite strait forward. However, as soon as Dropbox has to be installed for a second user, several workarounds are necessary.

#### Installing Dropbox the first time (for the first user)
1. The easiest way to install Dropbox on the workstation is from command line:

    If not already installed, we will install the `wget` package using the `apt` command:

        sudo apt-get update
        sudo apt-get install wget

    Dropbox cli version is available for both 32 and 64 bit editions, we will download Dropbox for our 64 bit system:

        cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -

    After executing this command, It will create a new hidden folder named `.dropbox-dist` in our `$HOME` directory.

    Run the following command to start Dropbox from the `.dropbox-dist` folder:

  ```
  ~/.dropbox-dist/dropboxd
  ```

  ```
  Output
  This computer isn't linked to any Dropbox account...
  Please visit https://www.dropbox.com/cli_link_nonce?nonce=35ff549233f01a5d4e699826b0ab6ffb to link this device.
  ```

    Now open the browser and navigate to the above link to connect system with your dropbox account (If you have problems to log-in, see workaround further down under [Installing Dropbox for all other users](#installing-dropbox-for-all-other-users)).

    Enter Dropbox username, password and click `Sign-in`.

    The server is connected to our dropbox account, we should see a message like below:

        This computer is now linked to Dropbox. Welcome Smart

    Folder named `Dropbox` will be created in our home directory. Keep the data in this folder to access it from any devices.

    Congratulations! The workstation has been linked to our Dropbox account. The Dropbox will keep running until we stop it by pressing `CTRL + C`. We should start it manually every time when we want to use Dropbox.

2. Installing Dropbox CLI:
    If Python is not already installed, we will firstly install it using the command command below:

        sudo apt install python

    Now, Download the dropbox python script and put it in our PATH, for example `/usr/local/bin/`:

        sudo wget -O /usr/local/bin/dropbox "https://www.dropbox.com/download?dl=packages/dropbox.py"

    Make it executable:

        sudo chmod +x /usr/local/bin/dropbox

    Now we can start using the dropbox cli. To display help, simply run:

        dropbox

    Output:

    ```
    Dropbox command-line interface

    commands:

    Note: use dropbox help <command> to view usage for a specific command.

      status       get current status of the dropboxd
      throttle     set bandwidth limits for Dropbox
      help         provide help
      stop         stop dropboxd
      running      return whether dropbox is running
      start        start dropboxd
      filestatus   get current sync status of one or more files
      ls           list directory contents with current sync status
      autostart    automatically start dropbox at login
      exclude      ignores/excludes a directory from syncing
      lansync      enables or disables LAN sync
      sharelink    get a shared link for a file in your dropbox
      proxy        set proxy settings for Dropbox
    ```

    To view the usage of a specific command, for example `running`, run:

        dropbox help running

    Output:

        dropbox running

        Returns 1 if running 0 if not running.

    To see if dropbox service is running or not. Type command below:

        dropbox status

    Output:

        Dropbox isn't running!

    To start Dropbox service. Type command below:

        dropbox start

    Let us again check if it is running using command:

        dropbox status

    Output:

        Up to date

    It will keep running until we reboot the system.

    To stop Dropbox service. Type command below:

        dropbox stop

    Output:

        Dropbox daemon stopped.

    To get the current sync status of a file, Type command below:

        dropbox filestatus Dropbox/smart.txt

    Output:

        Dropbox/smart.txt: up to date

    We can exclude a directory from syncing. For instance, we will exclude `dir1` folder using command below (best practice is to write the directory-path relative to the home-folder like: `~/Dropbox\ \(DSPlab\)/00_Team/03_Projects/Archiv/`):

        dropbox exclude add dir1

    We can add multiple directories with space separated values to exclude them from syncing like below:

        dropbox exclude add dir1 dir2

    To view the list of directories currently excluded from syncing, type command below:

        dropbox exclude list

    To remove a directory from the exclusion list, type command below:

        dropbox exclude remove dir2

    To get a shared link for a file, for example `smart.txt`, in our Dropbox folder, type command below:

        dropbox sharelink Dropbox/smart.txt

    Output:

        https://www.dropbox.com/s/rqteaol58c1zlkw/smart.txt?dl=0

    We can now pass the above URL to anyone.

    To enable lansync, type command below:

        dropbox lansync y

    To disable lansync, type command below:

        dropbox lansync n

3. Starting Dropbox automatically every reboot:
    We can make Dropbox service to automatically start on every reboot. Create a systemd service unit for Dropbox:

        sudo nano /etc/systemd/system/dropbox.service

    Add the following lines:

        [Unit]
        Description=Dropbox Service
        After=network.target

        [Service]
        ExecStart=/bin/sh -c '/usr/local/bin/dropbox start'
        ExecStop=/bin/sh -c '/usr/local/bin/dropbox stop'
        PIDFile=${HOME}/.dropbox/dropbox.pid
        User=smart
        Group=smart
        Type=forking
        Restart=on-failure
        RestartSec=5
        StartLimitInterval=60s
        StartLimitBurst=3

        [Install]
        WantedBy=multi-user.target

    Replace User, Group and dropbox cli path `/usr/local/bin/` with our own values. Save and quite the file.

    Reload daemon using command below:

        sudo systemctl daemon-reload

    Enable Dropbox service using command below:

        sudo systemctl enable dropbox

    Start Dropbox service using command below:

        sudo systemctl start dropbox

    Now Dropbox service will automatically start at every reboot.

    Check running of the service using command below:

        sudo systemctl status dropbox

4. Using Dropbox GUI on workstations:
    In our installation it was finally not clear, if the GUI is already installed under point 1.) or if it has to be installed separately. If it can't be found in the Kubuntu-menu, the `deb.` package has to be downloaded from the official Dropbox web-site and can be afterwards installed as followed:

        sudo apt install ./dropbox_2019.02.14_amd64.deb

    If we will start Dropbox afterwards we can see that Dropbox will not be able to verify binary signatures if python-gpgme is not installed. We can install python-gpgme by executing command below:

        sudo apt install python-gpgme

    Congratulations, you have installed Dropbox successfully!

#### Installing Dropbox for all other users
If Dropbox was already installed before for an other user on the workstation, not everything from before has to be done again! Following the necessary steps are listed:

1. Login as the user, for whom you want to install Dropbox.

2. Follow the steps under the point 1.) of [Installing Dropbox the first time (for the first user)](#installing-dropbox-the-first-time-for-the-first-user).

3. Congratulations, Dropbox should be installed successfully! Nevertheless, you could have problems to link the workstation with the appropriate Dropbox account. Is this the case, the problem could be most likely because the Dropbox app can't open the login-screen because of a bug! Therefore go to the `system-settings` of Kubuntu, search for `browser` and select `Applications`. Then you should see in the list under `Default Applications` the entry `Web Browser`. Click on it, choose the second option `in the following application:` and set for example Firefox as the default web-browser as shown in the figure below. Try to start Dropbox again and now it should be possible to login. Do not forget to reset afterwards the default browser setting in the system-settings!

![Default web-browser setting](./Images/defaultBrowserSetting.png)

### Installing PyCharm
PyCharm is a very powerful Python-IDE and was tested to work properly with this workstation-setup! The PyCharm community-version is totally sufficient and can be downloaded [here](https://www.jetbrains.com/de-de/pycharm/download/#section=linux).

1. After the `Communiti-version` was successfully downloaded, create an empty folder where you want to install PyCharm.

2. Extract the content of the `.tar.gz` file into your newly created folder.

3. Execute the file `pycharm.sh` in the subfolder `./bin`. Congratulations, you have successfully installed PyCharm!

#### Create Start-Menu-Launcher for PyCharm
If you don't want to start PyCharm always with the shell-script `pycharm.sh` under the subfolder `./bin`, you can create a start-menu-launcher. In PyCharm this is very easy and can be done by starting PyCharm and click under the `tools`-menu onto `Create Desktop Entry..`. It asks you, if you want to create a start-menu-launcher for all users or only for your own. Because we recommend to install PyCharm for each user separately, you have to create the start-menu-launcher only for your own user.

#### GPU-setup for Tensorflow
The best option is to follow the installation-instruction on the website of [Tensorflow](https://www.tensorflow.org/install/gpu) for Ubuntu 18.04. Because the versions of the drivers and applications in the instruction might change over time, we refer here directly to the corresponding website. The nvidia-driver might be already installed on the workstation and you have to make sure, not to downgrade it. So check before following the instruction, which version is installed and possibly update it:

    nvidia-smi

or

    apt search nvidia-driver

#### Matplotlib-setup for PyCharm
If you only install matplotlib over the package-manager of pyCharm, you will not be able to actually show some plots during running a python-script. What is needed in addition is to execute in a terminal the following command (you can also execute it directly in the terminal of pyCharm):

    sudo apt-get install python3-tk

Afterwards it is possible to show some plots in pyCharm when you include into the script after the plot-commands also the following line:

    plt.show(block=True)  # to let plot open

This line will halt the script till you close the opened plots and preventing therefore pyCharm of executing the complete script and closing afterwards all opened plots automatically.

#### Setup Ray-Cluster Computing
With the python-library `Ray` it is possible to heavily parallelize python code and even distribute the work-load over several machines. To setup a Ray-cluster, one of the
machines has to be setup as the cluster-head:

    ray start --head

If executed on the head-machine, it will give you as output the address and port of the machine as well as the password for this ray-cluster. This information has then to be used to start on the remote-machine a second ray-cluster-node:

    ray start --address='<address-of-head-node>' --redis-password='<password>'

If you use own python-modules, they have also to be on the remote machine and there in the *root temporary directory*. But better is to reset the *root temporary directory* where your scripts and modules are stored on the remote machine. This can be achieved by defining the *root temporary directory* when starting on the remote machine the ray-cluster-node:

    ray start --address='<address-of-head-node>' --redis-password='<password>' --temp-dir='<path-to-folder-where-own-scripts-and-modules-are-stored>'

#### Installing Geckodriver for Selenium
If you want to use the browser automation-library Selenium, you need to install the Geckodriver (If Firefox should be automated):

- visit https://github.com/mozilla/geckodriver/releases
- download the latest version of "geckodriver-vX.XX.X-linux64.tar.gz"
- unarchive the tarball (tar -xvzf geckodriver-vX.XX.X-linux64.tar.gz)
- give executable permissions to geckodriver (chmod +x geckodriver)
- move the geckodriver binary to /usr/local/bin or any location on your system PATH.


### Mounting NFS-directories from NAS
Our NAS has at the moment the following fix IP: 147.87.167.11.

**Firstly make sure that the NAS has NFS activated and the Workstation is allowed to connect to the NAS.** On a QNAP-NAS you have to enter the IP of the Workstation under the `NFS-Hostaccess settings`. To mount afterwards the NAS-directories permanently on the Workstation, do the following:

1. Install nfs-common:

        sudo apt-get install nfs-common

2. Create the following folder: `/mnt/NAS`.

3. Add the following line to the file under `/etc/fstab` (make sure the IP is correct in your case!):

        147.87.167.11:/ /mnt/NAS nfs defaults 0 0

4. Execute the following command:

        mount /mnt/NAS

### Mounting directories from NAS over SAMBA
We saw, that due to owner- and access-rights it is easier to mount the NAS-directories over SAMBA than over NFS:

1. Create the following folder: `/mnt/NAS`.

2. Add the following line to the file under `/etc/fstab` (make sure the IP is correct in your case!):

        //147.87.167.11/data  /mnt/NAS/ cifs  username=user,password=pvlab18,file_mode=0777,dir_mode=0777  0 0

3. Execute the following command:

        mount /mnt/NAS

Note: If you want to mount a directory directly and only temporarely in the terminal, execute the following command (the new NAS as an example):

        sudo mount -t cifs //147.87.167.17/data /mnt/NAS -o username=user,password=data,file_mode=0777,dir_mode=0777

### Mounting directories from BFH-Server
Directories from BFH-servers can be also mounted over SAMBA: 
1. Make sure, `CIFS Utils pkg` is installed. If not, install it as following:

    `sudo apt-get install cifs-utils`

2. Now create a mount-point for the directory you want to mount. For example:

    `sudo mkdir /mnt/local_share`

3. Add the following line to the file under `/etc/fstab` (make sure the server-address is correct in your case! `<user>` and `<pw>` you have to replace with your username and password at the BFH):

        //data-bi.bfh.ch/IndoorLoc  /mnt/local_share/ cifs  username=<user>,password=<pw>,file_mode=0777,dir_mode=0777  0 0

    Note: If you want to mount a directory directly and only temporarely in the terminal, execute the following command:

        sudo mount -t cifs -o user=<user>,file_mode=0777,dir_mode=0777 //data-bi.bfh.ch/IndoorLoc /mnt/local_share/



## Ettus RFNoC
Installed with PyBombs according to: https://kb.ettus.com/Getting_Started_with_RFNoC_Development with the following changes.

###  Changes
- PyBombs installation was run with sudo-
- rfnoc directory permission was changed to read/write for everyone (777) after sudo installation

## Trouble-shooting
### Various remarks
Unfortunately `Kubuntu` is by default a single-click operating-system. That means to open a file or a folder only one mous-click is needed. There exists in the `settings -> input devices` the possibility to change this behavior!

### Show all processes on system
To show all processes on the system, you can use the two following tools:

    top

or

    htop

`htop` shows beside a list of all processes on the system also detailed information about the CPU- and memory-usage.

Both commands have the option to show only processes of a specific user:

    top -u <username>

or

    htop -u <username>

### Count the number of certain processes of a specific user
With the following command it is possible to count for a specific user the number of processes, which contains a certain string in its command:

    top -b -u <username> -n1 | grep <search string> | wc -l

**-b:** Batch-mode, is needed for properly process the output in the pipeline

**-n1:** outputs all processes in one output and terminates afterwards `top`

**wc -l:** counts the number of lines in the output

## Useful Tools and Hints
### Shutter
Shutter is a useful tool to make print-screens from the entire screen as well as from certain parts of the screen. To install it, type the following command in the terminal:

    sudo apt-get install shutter

### Environmental Variables
Environment variables provide a way to influence the behaviour of software on the system. For example, the "LANG" environment variable determines the language in which software programs communicate with the user.

Environment variables consist of names that have values assigned to them. For example, on a typical system in the US we would have the value "en_US.UTF-8" assigned to the "LANG" variable.

The meaning of an environment variable and the format of its value are determined by the application using it. There are quite a few well-known environment variables for which the meaning and the format have been agreed upon and they are used by many applications.

#### Where to set Environmental Variables
There are mainly three different ways, how environmental variables can be set and each of them has an other scope:

1. The easiest way to set an environmental variable but with the scope only in the active terminal is with the command `export`, e.g. using

        export PATH=$PATH:/usr/src/hive/build/dist/bin/

    where `$PATH` is needed to get already set data in the environmental variable `PATH`. Therefore new data is concatenated to already existing one in an environmental variable with a colon in between.

2. If you want to set an environmental variable permanently for a specific user, you have to set it in the file `~/.pam_environment`, e.g. writing on a new lines

        PATH="/usr/src/hive/build/dist/bin/"

    If in the file the specific environmental variable already exists, you have to concatenate new entries with a colon:

        PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"

3. If you want to set an environmental variable system wide, you have to set it in the file `/etc/environment`. The way you write it is the same like for user wide environmental variables.


## Complete System Architecture
### System Overview
The complete system contains not only the workstation but also a NAS, a 10Gbit-Switch, a backup-storage, an USV and a power-switch to reset each single device in this architecture. In the following picture you can see an overview of this architecture.

![Overview System Architecture](./Images/blockdiagramm_Anlage.png)

### NAS-Setup
The NAS is connected with the workstation over a SFP+ 10G Ethernet interface and can be reached in the intranet over the address `\\pvnas01.bfh.ch` or `\\147.87.167.11`.

The login-data are:
- Username: `user`
- Password: `pvlab18`

The NAS can be configured over the Web-GUI in the intranet under `http://147.87.167.11:8080`.

The login-data are:
- Username: `dsvadmin`
- Password: `pvlab2018`

### USV-settings
The NAS and its 10GBit switch is protected with an USV against temporary power-losses. Under `External Devices->USV` in the settings on the Web-GUI of the NAS, it is at the moment configured for the so called `auto-protection-mode`. This mode will terminate after certain minutes of power-loss all running tasks and eject the hard disks (is adjustable and was set now to 3 minutes) and will re-initiate all services again if the power-supply is reestablished. This mode guaranties after the power-supply is reestablished again, that the NAS will continue to work and not remain switched off!

### Ethernet Power Switch
The power-supply for the NAS and the workstation can be controlled over a Ethernet power-switch. This is certainly useful in cases, when one of the devices do not response anymore and need a hard-reboot. The Ethernet power-switch can be reached in the intranet over the address `http://jlco-t215-pwrsw.bfh.ch` or `http://147.87.167.15`. This address is also accessible when connected over VPN to the BFH-network!

The login-data are:
- Username: `admin`
- Password: `admin`
